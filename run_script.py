#!/usr/bin/env python3
"""
Este script tiene como objetivo "traducir" la configuracion del modelo
de SCO SM24_100SFP_AH al modelo TN4224 o LIB4424.

Antes de ejecutar el script por primera vez es necesario crear las carpetas
"Input_Files" y "Output_Files" en este directorio:

mkdir Input_Files
mkdir Output_Files

> Input_Files: En este directorio deberan colocar los archivos de configuracion
que desean ser convertidos.

> Output_Files: En este directorio se almacenara la salida de configuracion
convertida.
"""
import os
from app.parser import parser_SM24_100SFP_AH
from app.jinja_template import create_sco_template
from app.utils import file_choise
from app.utils import list_input_files

__author__ = "Jean Carlos Oballe Sarmiento"
__version__ = "1.0.1"
__maintainer__ = "Jean Carlos Oballe Sarmiento"
__email__ = "jcsarmiento@teco.com.ar"
__status__ = "Production"


if __name__ == "__main__":

    input_dir = 'Input_Files'
    output_dir = 'Output_Files'
    templates_dir = 'app/Templates'
    root_files = list_input_files(".")

    print("Verificando carpetas necesarias...")
    if input_dir not in root_files['dirnames']:
        print(f'La carpeta {input_dir} no se encuentra. Debe crearla.')

    if output_dir not in root_files['dirnames']:
        print(f'La carpeta {output_dir} no se encuentra. Debe crearla.')

    abs_path_input_dir = os.path.abspath(input_dir)
    abs_path_output_dir = os.path.abspath(output_dir)
    abs_path_templates_dir = os.path.abspath(templates_dir)

    input_file = file_choise(
        abs_path_input_dir, "Selecciona el archivo de origen: ")

    if input_file:
        sco_template = file_choise(
            abs_path_templates_dir,
            "Selecciona el template para la conversion de salida: ")

        if sco_template:
            input_file_path = input_dir + '/' + input_file

            with open(input_file_path, 'r') as f:
                input_data = f.readlines()
                json_data = parser_SM24_100SFP_AH(input_data)
                translated_config = create_sco_template(
                    templates_dir, sco_template, json_data)
                translated_config_file_name = \
                    abs_path_output_dir + '/' + sco_template.replace(
                        '.j2', '') + '-' + json_data['hostname'] + '.conf'

            with open(translated_config_file_name, 'w') as f:
                f.write(translated_config)
                print(f"El template traducido se almaceno correctamente en {translated_config_file_name}")
