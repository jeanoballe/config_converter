#!/usr/bin/env python3
from jinja2 import Environment, FileSystemLoader


def create_sco_template(dir_path: str, template_name: str, params):
    load_template = FileSystemLoader(dir_path)
    env = Environment(loader=load_template)
    template_sco = env.get_template(template_name)

    sco_config = template_sco.render(params)

    return sco_config
