#!/usr/bin/env python3
import re
import json


def parser_SM24_100SFP_AH(command_output: list):

    def _basic_information():
        basic_info = {}
        for line in command_output:
            hostname = re.search(r'hostname\s*([^\s]+)', line)
            if hostname is not None:
                basic_info['hostname'] = hostname.group(1)

            location = re.search(r'snmp-server location\s*([^\s]+)', line)
            if location is not None:
                basic_info['location'] = location.group(1)

            ip_gateway = re.search(r'ip default-gateway\s*([^\s]+)', line)
            if ip_gateway is not None:
                basic_info['ip_gateway'] = ip_gateway.group(1)

            ip_mgmt = re.search(r'ip address\s*([0-9.]+)', line)
            if ip_mgmt is not None:
                basic_info['ip_mgmt'] = ip_mgmt.group(1)

        return basic_info

    def _vlan_db_information():
        vlan_lines_info = []
        vlan_db_flag = False
        for line in command_output:
            if vlan_db_flag:
                if '!' not in line:
                    if line != '\n':
                        vlan_lines_info.append(line)
                else:
                    vlan_db_flag = False

            if 'vlan database' in line:
                vlan_db_flag = True

        vlans_info = []
        for vlan_config in vlan_lines_info:
            vlan_info = {}

            vlan_id = re.search(r'vlan\s*([0-9]+)', vlan_config)
            if vlan_id is not None:
                vlan_info['vlan_id'] = int(vlan_id.group(1))

            name = re.search(r'name\s*([^\s]+)', vlan_config)
            if name is not None:
                vlan_info['name'] = name.group(1)

            vlans_info.append(
                dict(
                    vlan_id=vlan_info.get('vlan_id', None),
                    name=vlan_info.get('name', None)
                )
            )

        return vlans_info

    def _interface_information():
        interface_flag = False
        interfaces_info = []
        interface_info = {}
        interface_count = 0
        for line in command_output:

            if 'interface ethernet 1/' in line:
                interface_flag=True

            if interface_flag:
                if '!' not in line:
                    if line != '\n':
                        interface_id = re.search(r'interface ethernet 1/([0-9]+)', line)
                        if interface_id is not None:
                            interface_info['interface_id'] = int(interface_id.group(1))

                        description = re.search(r'description\s*([^\n]+)', line)
                        if description is not None:
                            interface_info['description'] = description.group(1)

                        if ' untagged' in line:
                            untagged = re.search(r'([0-9,-]+)', line)
                            if untagged is not None:
                                interface_info['untagged'] = untagged.group(1)


                        native_vlan = re.search(r'switchport native vlan\s*([0-9]+)', line)
                        if native_vlan is not None:
                            interface_info['native_vlan'] = native_vlan.group(1)

                        if ' tagged' in line:
                            tagged = re.search(r'([0-9,-]+)', line)
                            if tagged is not None:
                                interface_info['tagged'] = tagged.group(1)
                else:
                    interfaces_info.append(
                        dict(
                            interface_id=interface_info.get('interface_id', None),
                            description=interface_info.get('description', None),
                            untagged_vlans=interface_info.get('untagged', None),
                            tagged_vlans=interface_info.get('tagged', None),
                            native_vlan=interface_info.get('native_vlan', None)
                        )
                    )
                    interface_info = {}
                    interface_count += 1
                    if interface_count == 28:
                        interface_flag = False

        return interfaces_info

    def _classify_interfaces(interface_info):
        interfaces = {
            'giga': [],
            'tengiga': [],
            'uplink': []
        }
        for interface in interface_info:
            if interface['description'] is None:
                interface['description'] = 'LIBRE'

            if interface['native_vlan'] is None:
                interface['native_vlan'] = "4000"
                if interface['tagged_vlans'] is None:
                    interface['tagged_vlans'] = "4000"
                else:
                    interface['tagged_vlans'] = f"{interface['tagged_vlans']},4000"
            else:
                if interface['tagged_vlans'] is None:
                    interface['tagged_vlans'] = interface['native_vlan']
                else:
                    interface['tagged_vlans'] = f"{interface['tagged_vlans']},{interface['native_vlan']}"

            if interface['interface_id'] < 25:
                interfaces['giga'].append(interface)
            else:
                if interface['description']:
                    if 'FC-UPLINK-PORT' in interface['description']:
                        interface['interface_id'] -= 24
                        interfaces['uplink'].append(interface)
                    else:
                        interface['interface_id'] -= 24
                        interfaces['tengiga'].append(interface)
                else:
                    interface['interface_id'] -= 24
                    interfaces['tengiga'].append(interface)

        return interfaces

    basic_info = _basic_information()
    vlan_db_info = _vlan_db_information()
    interface_info = _interface_information()
    interfaces = _classify_interfaces(interface_info)

    data = {
        'hostname': basic_info['hostname'],
        'location': basic_info['location'],
        'ip_gateway': basic_info['ip_gateway'],
        'ip_mgmt': basic_info['ip_mgmt'],
        'vlan_database': vlan_db_info,
        'interfaces': interfaces
    }

    return data


if __name__ == "__main__":
    with open('Input_Files/sm24_testing.conf', 'r') as f:
        data = f.readlines()

    json_data = parser_SM24_100SFP_AH(data)
    print(json.dumps(json_data, indent=4))
