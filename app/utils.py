#!/usr/bin/env python3
from os import walk


def list_input_files(dir_path):
    f = []
    for (dirpath, dirnames, filenames) in walk(dir_path):
        f.extend(filenames)
        break
    return {'filenames': filenames, 'dirnames': dirnames, 'dirpath': dirpath}


def file_choise(dirname, msg):
    choise = True
    archivo = list_input_files(dirname)['filenames']
    options = []
    if archivo:
        while choise:
            print(msg)
            for (i, file) in enumerate(archivo):
                print(f'Opcion {i} ==> Archivo {file}')
                options.append(i)

            option = input("Opcion:")

            if option.isnumeric():
                if int(option) in options:
                    choise = False
                    return archivo[int(option)]
                else:
                    options = []
                    print("La opcion ingresada es incorrecta")
            else:
                options = []
                print("La opcion ingresada es incorrecta")
    else:
        print(f'La carpeta {dirname} no tiene archivos.')
