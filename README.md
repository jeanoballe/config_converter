Este script tiene como objetivo "traducir" la configuracion del modelo de SCO SM24_100SFP_AH al modelo TN4224 o LIB4424.

Antes de ejecutar el script por primera vez es necesario crear las carpetas "Input_Files" y "Output_Files" en este directorio:

mkdir Input_Files
mkdir Output_Files

Input_Files: En este directorio deberan colocar los archivos de configuracion que desean ser convertidos.
Output_Files: En este directorio se almacenara la salida de configuracion convertida.